'use strict'
// - - - SET PORT - - -
const port = 5000
// - - - REQUIRE NODE MODULES - - -
const express = require('express')
const app = express()
const expressNunjucks = require('express-nunjucks')
const isDev = app.get('env') === 'development'
const path = require('path')
const morgan = require('morgan')
const flash = require('connect-flash')
const passport = require('passport')
const bodyParser = require('body-parser')
const session = require('express-session')
const SQLiteStore = require('connect-sqlite3')(session)
const favicon = require('serve-favicon')
const fileUpload = require('express-fileupload')

// - - - USE A FAVICON - - -
app.use(favicon(path.join(__dirname, 'public/favicon', 'favicon.ico')))

// - - - INITIALIZE PASSPORT - - -
const initializePassport = require('./config/passport-config.js')
initializePassport(
  passport,
  (user_name) => users.find((user) => user.user_name === user_name),
  (user_id) => users.find((user) => user.user_id === user_id),
)

// - - - INITIALIZE RENDER ENGINE NUNJUCKS - - -
const njk = expressNunjucks(app, {
  watch: isDev,
  noCache: isDev,
  autoescape: true,
})

// - - - APP SET VIEW - - -
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', expressNunjucks)

// - - - APP USE - - -
app.use(express.static('public'))
app.use(morgan('dev')) //prints every client request in console
app.use(fileUpload())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))

app.use(
  session({
    secret: 'fmlIhdhuthblGlmdmB',
    cookie: { maxAge: 600000 }, // In milliseconds from Date.now()  
    store: new SQLiteStore({ dir: './data/', db: 'blog.db' }),
    resave: false,
    saveUninitialized: false,
  }),
)

app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

// - - - INITIALIZE FLASH MESSAGES - - -
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res)
  res.locals.success_msg = req.flash('success_msg')
  res.locals.error_msg = req.flash('error_msg')
  res.locals.error = req.flash('error')
  next()
})

app.use(function (req, res, next) {
  res.locals.isAuthenticated = req.isAuthenticated()
  next()
})

// - - - PROGRAMM STARTS INDEX ROUTER - - -
app.use('/', require('./routes/index.js'))

// - - - PROGRAMM STARTS BLOG ROUTER - - -
app.use('/entry', require('./routes/blog.js'))

// - - - APP LISTEN ON PORT - - -

app.listen(port, () => console.log('Server listen on Port ' + port))

// - - - ERROR HANDLER - - -

app.use(function (req, res, next) {
  res.status(404)
  res.render('error.twig', {
    error: `Diese Seite existiert nicht! Bitte überprüfen Sie die URL.`,
  })
})

app.use(function (req, res, next) {
  res.status(500)
  res.render('error.twig', {
    error: `Entschuldigung, hier ist etwas schief gelaufen. Versuche es später noch einmal.`,
  })
})
module.exports = app
