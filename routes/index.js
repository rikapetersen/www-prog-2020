'use strict'

// - - - REQUIRE NODE MODULES - - -
const express = require('express')
const router = express.Router()
const passport = require('passport')

// - - - REQUIRE OTHER CLASSES - - -
const { validationResult } = require('express-validator/check')
const { validateChecks } = require('../config/validation.js')
const logInController = require('../controllers/loginController.js')
const db = require('../db.js')

// - - - ROUTER GET - - -
router.get('/', (req, res) => {
  res.render('../views/home.twig', {
    user: req.user,
  })
  console.log('--- Show Indexpage ---')
})

router.get('/alphabetisch',
  (req, res) => {
    controller.showAllEntrysAlpha(db, req, res, {
      user: req.user,
    })
    console.log('--- Show Indexpage Alphabetic ---')
  })

router.get('/dokumentation', (req, res) => {
  res.render('../views/dokumentation.twig',  {
    user: req.user,
  })
  console.log('--- Show Documentation ---')
})

router.get('/impressum', (req, res) => {
  res.render('../views/impressum.twig', {
    user: req.user,
  })
  console.log('--- Show Impressum ---')
})

router.get('/registration', (req, res) => {
  logInController.showRegistrationForm(db, req, res)
})

router.get('/logIn', (req, res) => {
  logInController.showLogInForm(db, req, res)
})

router.get('/logOut', (req, res, next) => {
  console.log('--- Log Out ---')
  logInController.logOutAction(req, res)
})

// - - - ROUTER POST - - -
router.post('/registration', validateChecks, (req, res) => {
  logInController.postRegistrationAction(db, req, res, validationResult(req))
})

router.post('/logIn', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',   
    failureRedirect: '/logIn',
    failureFlash: true,
  })(req, res, next)

  console.log('--- POST LOGIN ---')
})

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/logIn')
}

function checkAuthentificationStatus(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect('/')
  }
  next()
}

module.exports = router
