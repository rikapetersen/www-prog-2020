'Use Strict'

const bcrypt = require('bcryptjs')
const userDatabase = require('../models/userDatabase.js')

module.exports = {
  // - - - SHOW REGISTRATION FORM - - -
  showRegistrationForm: function (db, req, res) {
    console.log('--- Show Register Form ---')
    res.render('registration.twig', {})
  },

  // - - - SHOW LOGIN FORM - - -
  showLogInForm: function (db, req, res) {
    console.log('--- Show Login Form ---')
    res.render('logIn.twig', {})
  },

  logOutAction: function (req, res) {
    console.log('--- Log Out Action ---')
    req.user = undefined
    res.locals.authenticated = undefined
    res.clearCookie('connect.sid', { domain: '', path: '/' })
    req.session.destroy(function (err) {
      res.redirect('/')
    })
  },

  // - - - POST REGISTRATION - - -
  postRegistrationAction: function (db, req, res, errors) {
    console.log('--- Post Register Form ---')
    const registerData = req.body
    console.log(registerData)

    if (!errors.isEmpty()) {
      let error = 'Es wurden ungültige Daten eingegeben!'

      res.render('registration.twig', {
        error: error,
        errors: errors.array(),
      })
    } else {
      bcrypt.genSalt(10, (error, salt) => {
        // auf 10 lassen, 10 ist Defaultwert und höhere Zahlen brauchen länger
        bcrypt.hash(registerData.user_password, salt, (error, hash) => {
          if (error) throw error

          registerData.user_password = hash
          console.log('Gehashtes Password: ' + registerData.user_password)

          userDatabase.registrationOfUser(db, registerData, (next) => {
            req.flash(
              'info',
              'Du hast dich erfolgreich registriert. Logge dich nun mit deinen Daten ein.',
            )
            res.redirect('logIn')
          })
        })
      })
    }
  },
}
