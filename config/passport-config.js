const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
const userDatabase = require('../models/userDatabase.js')
const db = require('../db.js')

function initialize(passport) {
  const authenticateUser = (user_name, user_password, done) => {
    // - - - CHECK IF USER IS IN DATABASE  - - -
    userDatabase.getUserSql(db, user_name, (user) => {
      if (!user[0]) {
        console.log('--- Falscher Username ---')
        return done(null, false, {
          message:
            'Mit diesem Username ist kein Nutzer angemeldet! Überprüfe deine Eingabe.',
        })
      }

      // - - - COMPARE INPUT PASSWORD WITH DATABASEPASSWORD - - -
      bcrypt.compare(user_password, user[0].user_password, function (
        err,
        isMatch,
      ) {
        if (err) throw err
        if (isMatch) {
          console.log('--- Passwort isMatch ---')
          return done(null, user[0])
        } else {
          console.log('--- Passwort is Not Match ---')
          return done(null, false, {
            message:
              'Das Passwort wurde falsch eingegeben! Überprüfe deine Eingabe.',
          })
        }
      })
    })
  }

  // - - - GET INPUTFIELDS OF LOGINFORM AND START AUTHENTIFICATE FUNCTION - - -
  passport.use(
    new LocalStrategy(
      { usernameField: 'user_name', passwordField: 'user_password' },
      authenticateUser,
    ),
  )

  // - - - SERIALIZATION AND DESERIALIZATION OF USER - - -
  passport.serializeUser((user, done) => done(null, user.user_id))

  passport.deserializeUser(function (id, done) {
    userDatabase.getUserByIdSql(db, id, (user) => {
      done(null, user[0])
    })
  })
}

module.exports = initialize
