'use strict'
// - - - REQUIRE NODE MODULES - - -
const util = require('util')
const express = require('express')
const multer = require('multer')
const router = express.Router()

// - - - REQUIRE OTHER CLASSES - - -
const controller = require('../controllers/controller.js')
const formController = require('../controllers/formController.js')
const db = require('../db.js')

// - - - INIT MULTER STORAGE -
var multerStorage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, path.join(__dirname, 'public/pictures'))
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + '_' + file.originalname)
  },
})

var upload = multer({ storage: multerStorage })

// - - - ROUTER GET - - -
router.get('/:id', (req, res) => {
  if (req.params.id === 'add') {
    if (req.isAuthenticated()) {
      console.log('--- blog.js --- routing get ADD ---')
      formController.showFormAction(db, req, res)
    } else {
      res.render('error.twig', {
        error: `Diese Funktion ist nur für authentifizierte User. Sie sind nicht authentifiziert. Bitte Loggen Sie sich ein oder Registrieren sie sich, um diese Funktion nutzen zu können.`,
      })
    }
  } else if (req.params.id === 'all') {
    console.log('--- Show All Entrys ---')
    controller.showAllEntrys(db, req, res)
  } else if (req.params.id === 'alphabetisch') {
    console.log('--- Show All Entrys Alphabetic ---')
    controller.showAllEntrysAlpha(db, req, res)
  } else {
    controller.showID(db, req.params.id, req, res)
  }
})

router.get('/:id/edit', (req, res) => {
  console.log('--- Edit Post ---')
  formController.showFormAction(db, req, res)
})

router.get('/profile/:id', (req, res) => {
  controller.getAllEntrysOfCurrentUser(db, req.params.id, req, res)
})

router.get('/:id/comment', (req, res) => {
  console.log('--- Comment Post ---')
  if (req.isAuthenticated()) {
    formController.showComment(db, req, res, req.params.id)
  } else {
    res.render('error.twig', {
      error: ` Diese Funktion ist nur für authentifizierte User. Sie sind nicht authentifiziert. Bitte Loggen Sie sich ein oder Registrieren sie sich, um diese Funktion nutzen zu können.`,
    })
  }
})

router.get('/:id/:comment_id/delete', (req, res) => {
  console.log('DELETE COMMENT')
  formController.deleteCommment(db, req, res, req.params.comment_id)
})

// - - - ROUTER POST - - -

/* 
//********************************************************************************
//  - - - TRY TO MAKE A FILE UPLOADER WITH MULTER - - -

router.post('/add', upload.single('picture'), function (req, res, next) {
  console.log('Router.Post Blog.js')
  const fileData = req.file
  console.log(
    '--- BILDNAME --- ' + `${util.inspect(req.file.originalname, false, null)}`,
  )
  //console.log("BILDNAME: " + fileData.originalname);
  formController.postFormAction(db, req, res)
})
//********************************************************************************
*/

router.post('/add', (req, res, next) => {
  console.log('Router.Post Blog.js')
  // console.log(
  //   '--- BILDNAME --- ' + `${util.inspect(req.files.originalname, false, null)}`,
  // )
  formController.postFormAction(db, req, res)
})

router.post('/:id/comment', (req, res) => {
  console.log('Post Comment ')
  formController.postComment(db, req, res, req.params.id)
})

router.post('/:id/edit', (req, res) => {
  console.log('Edit Comment ')
  formController.postFormAction(db, req, res, req.params.id)
})
router.post('/addComment', (req, res) => {
  console.log('Add Comment ')
  formController.postFormAction(db, req, res)
})

module.exports = router
