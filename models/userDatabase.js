'Use Strict'
const util = require('util')
let sql

module.exports = {
  // - - - SQL - - -
  // - - - REGISTRATION SQL - - -
  registrationOfUser: function (db, user, callback) {
    console.log("--- Registration of User ---"); 
    sql = "INSERT INTO Users (user_name, user_email, user_password) VALUES ($user_name, $user_email, $user_password)"
    this.changeUser(db, [], user, callback)
  },

  // - - - GET USER SQL - - -
  getUserSql: function (db, user, callback) {
    console.log("--- Get User SQL ---"); 
    sql = "SELECT * FROM Users WHERE Users.user_name = $Users"
    this.getUser(db, user, callback)
  },

  // - - - GET A USER BY ID SQL - - -
  getUserByIdSql: function (db, id, callback) {
    console.log("--- Get User by ID SQL ---"); 
    console.log("--- USER ID: " + id)
    sql = "SELECT * FROM Users WHERE Users.user_id = $id"
    this.getUserbyId(db, id, callback)
  },
  // - - - GET A USER BY ID - - -
  getUserbyId: function (db, id, callback) {
    console.log("--- Get User by ID---"); 
    db.all(sql, { $id: id }, function (err, rows) {
      if (err) {
        throw err
      }
      callback(rows)
      console.log('--- Get User By ID' +`${util.inspect(rows, false, null)}` + ' ---')
    })
  },

  // - - - GET A USER - - -
  getUser: function (db, user, callback) {
    console.log("--- Get User ---"); 
    db.all(sql, { $Users: user }, function (err, rows) {
      if (err) {
        throw err
      }
      callback(rows)
    })
  },

  // - - - CHANGE THE USER - - -
  changeUser: function (db, id, data, callback) {
    console.log("--- Change User ---"); 
    db.run(
      sql,
      {
        $user_name: data.user_name,
        $user_email: data.user_email,
        $user_password: data.user_password,
       
      },
      function (error) {
        if (error) {
          throw error
        }
        callback()
      },
    )
  },
}
