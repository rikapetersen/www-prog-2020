'use strict'

const sqlite3 = require('sqlite3').verbose()
let db = new sqlite3.Database('./data/blog.db')

// - - - CREATE DB TABLES AND CREATE CONTENT - - -
db.serialize(function () {
  db.run(
    'CREATE TABLE Users (user_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,user_name VARCHAR(15) NOT NULL, user_email VARCHAR(100) NOT NULL, user_password INTEGER NOT NULL)',
  )

  db.run(
    "INSERT INTO Users (user_name, user_email, user_password) VALUES ('HansWilhelm','hanswilhelm@web.de','HansWilhelm123'),  ('Frida',  'frida@web.de', 'Passwort123') , ('AnNaLoUiSe' , 'annalouise@gmail.com', 'HalloPasswort22.' )",
  )

  db.run(
    'CREATE TABLE Entrys (entry_id INTEGER PRIMARY KEY AUTOINCREMENT,entry_title	TEXT,entry_text	TEXT,entry_author_fk INTEGER,entry_author_username TEXT,picture TEXT,FOREIGN KEY(entry_author_fk) REFERENCES USERS(user_id))',
  )

  db.run(
    "INSERT INTO Entrys (entry_author_fk, entry_title, entry_text, entry_author_username, picture) VALUES ('1','!!Seht euch dieses Süße Bild an!!','Das ist mein kleines Kätzchen. Ziemlich süß was? Sie ist wirklich ein ganz besonderes Kätzchen und ich habe sie sehr lieb. Knuddelige Grüße', 'HansWilhelm' ,'cat.jpg'), ('2','Neues Lauchsuppen Rezept.', 'Ich habe vor kurzem ein neues Rezept für Lauchsuppe ausprobiert. Es schmeckt super lecker und ist ganz einfach nach zu kochen. Ihr braucht dafür 3 Stangen Lauch, 2 Liter Gemüsebrühe, Ein Packet Schmelzkäse, veganes Hack und einen becher Sahne. Viel Spaß beim nachkochen.', 'Frida' ,'soup.jpg' ), ('1','Spendenaktion für das Tierheim','Das Tierheim in Musterstadt ist auf unser Spenden angewiesen. jetzt noch viel mehr als sonst. Gerne nehmen sie auch Futterspenden oder Spielzeugspenden.', 'HansWilhelm' ,'tierheim.jpg'  ) , ('3', 'Neue Siebträgermaschine', 'Hey ihr, ich habe mir vor kurzem eine neue Siebträgermaschine gekauft. Nun benötige ich eine Möglichkeit das Wasser zu entkalken. Habt ihr da Erfahrungen gemacht und könnt mir eine Möglichkeit empfehlen? Falls ihr auch noch Espresso-Bohnen Empfehlungen für mich habt, dann könnt ihr sie mir auch gerne mitteilen. Mfg AnnaLouise', 'AnNaLoUiSe', 'coffee.jpg'), ('2', 'Mein super süßer Hamster ♥', 'Ich habe hier ein neues Bild von meinem hamster. er heißt Teddy. Ist er nicht süß? Er ist mein liebstes Fotomodell. Weitere Bilder folgen. Eure Frida', 'Frida', 'Hamster.jpg' ) ",
  )

  db.run(
    'CREATE TABLE Comments (comment_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, comment_text TEXT, comment_user_fk INTEGER, comment_entry_fk	INTEGER, comment_username	TEXT, FOREIGN KEY(comment_user_fk) REFERENCES USERS(user_id))',
  )

  db.run(
    "INSERT INTO Comments ( comment_text,  comment_user_fk,  comment_entry_fk, comment_username)  VALUES ('Toll!! So ein niedliches Kätzchen. Gerne mehr Bilder!!!!',  2, 1, 'Frida'), ('Gefällt mir sehr!!! Gab es gestern bei uns zum Mittag, sogar die Kinder mochten es.',  1, 2, 'HansWilhelm'), ('Oh wie süß er ist *__*', 3, 5, 'AnNaLoUiSe'), ('Ich liebe Katzen!!!', 3,1, 'AnNaLoUiSe'), ('Ich habe mir im Internet Entkalkungssäckchen besorgt, die man in den Wassertank der Maschine legen kann. Funktioniert super.', 1, 4, 'HansWilhelm'), ('Das ist ein sehr niedlicher Hamster.', 1, 5, 'HansWilhelm'), ('Toll, dass du dich so dafür engagierst.',  2, 3, 'Frida'), ('Zum Glück hat das Tierheim Menschen wie dich. ', 3, 3, 'AnNaLoUiSe')",
  )
})

db.close()
