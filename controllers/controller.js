'use strict'

const blogdb = require('../models/entryDatabase.js')

module.exports = {
  // - - - SHOW ALL BLOGENTRYS - - -
  showAllEntrys: function (db, req, res) {
    console.log('--- Show all im Controller ---')
    blogdb.showAllEntrys(db, (entry) => {
      console.log('--- Entry ID in Controller: ' + entry[0].id + ' ---')
      res.render('entrys.twig', {
        user: req.user,
        all: entry,
      })
    })
  },

  showAllEntrysAlpha: function (db, req, res) {
    blogdb.showAllEntrysAlpha(db, (entry) => {
      res.render('entrys.twig', {
        user: req.user,
        all: entry,
      })
    })
  },

  // - - - SHOW BLOGENTRY BY ID - - -
  showID: function (db, id, req, res) {
    blogdb.showEntryByID(db, id, (entry) => {
      // console.log("comments");
      console.log(entry)
      res.render('showEntryById.twig', {
        user: req.user,
        comments: entry,
        entry: entry[0],
      })
    })
  },

  // - - - SHOW ALL ENTRYS OF THE CURRENT USER - - -
  getAllEntrysOfCurrentUser: function (db, id, req, res) {
    blogdb.showAllEntrysOfUser(db, id, (entry) => {
      if (entry[0] == null) {
        res.render('userProfile.twig', {
          user: req.user,
          all: null,
        })
      } else {
        res.render('userProfile.twig', {
          user: req.user,
          all: entry,
          author: entry[0].entry_author_username,
          
        })
      }
    })
  },
}
