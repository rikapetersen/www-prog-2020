'use Strict'
const util = require('util')
let sql

module.exports = {
  // - - - SQL - - -
  // - - - SHOW ALL ENTRYS SQL  - - -
  showAllEntrys: function (db, callback) {
    console.log('ShowAll entryDatabase ')
   sql = `SELECT * FROM ENTRYS ORDER BY Entrys.entry_id DESC` 
   this.getEntry(db, [], callback)
  },

  // - - - SHOW ALL ENTRYS OF A USER SQL - - -
  showAllEntrysOfUser: function (db, id, callback) {
    sql = `SELECT * from Entrys Where Entrys.entry_author_fk = $id `
    this.getEntry(db, id, callback)
  },

  showAllEntrysAlpha: function (db, callback) {
    sql = `SELECT * FROM ENTRYS ORDER BY Lower(entry_title)`
    this.getEntry(db, [], callback)
  },

  // - - - SHOW ENTRY BY ID SQL - - -
  showEntryByID: function (db, id, callback) {
    sql = `SELECT * FROM Entrys LEFT JOIN Comments ON Comments.comment_entry_fk = Entrys.entry_id LEFT JOIN Users ON Entrys.entry_author_fk = Users.user_id WHERE Entrys.entry_id = $id`
    console.log('ID:' + id)
    this.getEntry(db, id, callback)
  },

  // - - - EDIT ENTRY SQL - - -
  editEntry: function (db, id, entry, callback) {
    sql = `UPDATE ENTRYS SET entry_author_fk = $entry_author_fk, entry_title = $entry_title, entry_text = $entry_text, entry_author_username = $entry_author_username, picture = $picture WHERE Entrys.entry_id = $id`
    this.changeEntry(db, id, entry, callback)
  },

  // - - - WRITE ENTRY SQL - - -
  createEntry: function (db, entry, callback) {
    sql = `INSERT into ENTRYS (entry_author_fk, entry_title, entry_text, entry_author_username, picture ) VALUES ( $entry_author_fk, $entry_title, $entry_text, $entry_author_username, $picture)`
    this.changeEntry(db, [], entry, callback)
  },

  // - - - DELETE AN ENTRY SQL - - -
  deleteEntry: function (db, id, callback) {
    sql = `DELETE FROM ENTRYS WHERE Entrys.entry_id = $id`
    this.changeEntry(db, id, [], callback)
  },

  // - - - WRITE A COMMENT SQL - - -
  writeComment: function (db, id, data, callback) {
    console.log('writeComment Funktion in EntryDatabase')
    sql = `INSERT INTO Comments(comment_text , comment_user_fk, comment_entry_fk, comment_username) VALUES ($comment_text, $comment_user_fk, $comment_entry_fk, $comment_username)`
    console.log('WriteComment Funktion im Model nach der SQL Abfrage')
    this.changeComment(db, id, data, callback)
  },

  // - - - DELETE A COMMENT SQL - - -
  deleteComment: function (db, id, callback) {
    console.log('--- Delete Comment ---')
    console.log('Commen ID  ' + `${util.inspect(id, false, null)}`)
    sql = `DELETE FROM Comments WHERE Comments.comment_id = $id`
    db.run(
      sql,
      {
        $id: id,
      },
      function (err) {
        if (err) {
          throw err
        }
        callback()
      },
    )
  },
  // - - - GET ENTRY - - -
  getEntry: function (db, id, callback) {
    console.log('ID außen: ' + id)

    db.all(sql, { $id: id }, function (err, rows) {
      console.log('SQL: ' + sql)
      console.log('ID: ' + id)

      if (err) {
        throw err
      }
      console.log('ROWS: ' + rows)
      callback(rows)
      console.log('ROWS ' + `${util.inspect(rows, false, null)}`)
    })
  },

  // - - - CHANGE A COMMENT - - -
  changeComment: function (db, id, comment, callback) {
    console.log('change Comment')
    db.run(
      sql,
      {
        $comment_text: comment.comment_text,
        $comment_user_fk: comment.comment_user_fk,
        $comment_username: comment.comment_username,
        $comment_entry_fk: id,
      },

      function (err) {
        if (err) {
          throw err
        }
        console.log('erfolgreich')
        callback()
      },
    )
  },

  // - - - CHANGE A ENTRY - - -
  changeEntry: function (db, id, entry, callback) {
    console.log(' --- Change Entry Model  --- ')
    console.log('--- Entry im Model: ' + entry + ' ---')

    db.run(
      sql,
      {
        $entry_author_fk: entry.entry_author_fk,
        $entry_title: entry.title,
        $entry_text: entry.text,
        $entry_author_username: entry.entry_author_username,
        $picture: entry.picture,
        $id: id,
      },
      console.log('--- SQL Change Entry --- Model --- ' + sql),
      function (err) {
        if (err) {
          throw err
        }
        callback()
      },
    )
  },
}
