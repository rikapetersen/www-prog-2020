// - - - CONNECT TO DATABASE - - -

const sqlite3 = require('sqlite3').verbose()

let db = new sqlite3.Database('./data/blog.db', (err) => {
  if (err) {
    return console.error(err.message)
  }
  console.log('Database is connected')
})

module.exports = db
