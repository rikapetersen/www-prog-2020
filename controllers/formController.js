'use strict'

const util = require('util')
const entryDatabase = require('../models/entryDatabase.js')
let entryData = []

module.exports = {
  // - - - SHOW FORM - - -
  showFormAction: function (db, req, res) {
    console.log('--- showFormAction --- FormController')
    if (req.params.id === 'add') {
      console.log('--- showFormAction --- FormController --- Add')
      return res.render('createEntry.twig', {
        user: req.user,
      })
    }
    if (req.params.id) {
      entryDatabase.showEntryByID(db, req.params.id, (rows) => {
        console.log('--- ROWS: ' + rows)

        entryData = rows[0]

        console.log('--- EntryData: ' + entryData)
        console.log('--- ENTRYDATA.ENTRY_AUTHOR_FK = ' + entryData.entry_author_fk)

        if (req.user != null) {
          if (req.user.user_id == entryData.entry_author_fk) {
            console.log('--- FORM CONTROLLER --- IS AUTHOR')
            res.render('editEntry.twig', {
              user: req.user,
              entry: entryData,
            })
          } else {
            console.log('--- FORM CONTROLLER --- IS NOT AUTHOR')
            res.render('error.twig', {
              user: req.user,
              error: `Sie sind zu dieser Handlung nicht befugt!`,
            })
          }
        } else {
          console.log('--- FORM CONTROLLER --- IS NOT AUTHORIZED')
          res.render('error.twig', {
            error: `Sie sind zu dieser Handlung nicht befugt!`,
          })
        }
      })
    }
  },

  // - - - POST FORM - - -
  postFormAction: function (db, req, res, id) {
    const formData = req.body
    const file = req.file

    console.log('--- Body: ' + formData)
    console.log('--- File: ' + file)

    // const {
    //   valid,
    //   validFormData: formData,
    //   validFiles: formFiles
    // } = validateFormData(req.body, req.file);

    // const preparedData = prepareFormData(formData, formFiles);

    formData.entry_author_fk = req.user.user_id
    formData.entry_author_username = req.user.user_name

    console.log((formData.entry_author_username = req.user.user_name))
    console.log(
      '--- PostFormAction --- ' + `${util.inspect(formData, false, null)}`,
    )

    if (formData.delete) {
      console.log('--- Delete Entry')
      entryDatabase.deleteEntry(db, id, (next) => {
        req.flash( 'info','Du hast den Eintrag erfolgreich gelöscht');
        res.redirect('/')
      })
      return
    }

    console.log(' --- Validate Data Bool ' + validateForm(formData) + ' ---')

    if (validateForm(formData)) {
      console.log('--- postFormAction --- valide formData')

      if (formData.create) {
        console.log(' --- formData.create --- ')
        console.log(
          'PostFormAction --- ' + `${util.inspect(formData, false, null)}`,
        )

        entryDatabase.createEntry(db, formData, (next) => {
          req.flash( 'info','Du hast erfolgreich einen Eintrag erstellt.');
          res.redirect('/')
        })
      } else if (formData.update) {   
        entryDatabase.editEntry(db, id, formData, (next) => {
          req.flash( 'info','Du hast den Eintrag erfolgreich bearbeitet');
          res.redirect('/')
        })
      }
    } else {
      console.log('Error')
      let error = 'Achtung, es wurden ungültige Daten eingegeben.'
      let form = 'editEntry.twig'

      res.render(form, {
        entry: entryData,
        error: error,
      })
    }
  },

  // - - - SHOW COMMENT FORM - - -
  showComment: function (db, req, res, id) {
    res.render('writeComment.twig', {
      user: req.user,
    })
  },

  // - - - POST COMMENT FORM - - -
  postComment: function (db, req, res, id) {
    console.log('postComment')
    let commentData = req.body

    commentData.comment_user_fk = req.user.user_id
    commentData.comment_username = req.user.user_name // GET NAME OF USER

    console.log('--- commentData' + commentData)
    

    entryDatabase.writeComment(db, id, commentData, (comment) => {
      req.flash( 'info','Du hast einen Kommentar geschrieben.');
      res.redirect('/entry/' + id)
    })
  },

  // - - - DELETE COMMENT - - -
  deleteCommment: function (db, req, res, id) {
    console.log('--- delete Comment ---')
    entryDatabase.deleteComment(db, id, (comment) => {
      req.flash( 'info','Das Kommentar wurde gelöscht');
      res.redirect('/entry/' + req.params.id)
    })
  },
}

// - - - VALIDATE FORM - - -
function validateForm(formData) {
  console.log('--- ValidateForm --- ' + formData)

  return (
    // CHECK IF FORMDATAT IS NOT EMPTY
    formData.title !== null &&
    formData.title.length > 0 &&
    formData.text !== null &&
    formData.text.length > 0
  )
}

// - - - VALIDATE FORM DATA AND FILES - - -
function validateFormData(formData, formFiles) {
  console.log('--- validateFormData ---')
  let valid = true
  let errors = {}
  if (typeof formData !== 'object') {
    return { valid: false, formData: formData, formErrors: errors }
  }
  const fileErrors = []

  if (formFiles.picture !== undefined) {
    if (formFiles.picture.truncated) {
      valid = false
    }
    if ((formFiles.picture.mimetype !== 'image/' + jpg) | png | jpeg) {
      // CHECK MIMETYPE OF FILE
      valid = false
    }
  }
  if (fileErrors.length > 0) {
    errors.file = fileErrors
  }
  return { valid: valid, validFormData: formData, validFiles: formFiles }
}

// - - - PREPARE FORMDATA AND FORMFILES FOR DATABASE - - -

function prepareFormData(formData, formFiles) {
  console.log('prepareFormData')
  let preparedData, picData
  let standartData = {
    author_fk: formData.author_fk,
    title: formData.title,
    text: formData.text,
  }
  if (formFiles.picture === undefined) {
    preparedData = Object.assign({}, standartData)
    return preparedData
  } else {
    formFiles.picture.mv('./public/pictures/' + formFiles.picture.name) // MOVE PICTURE INTO PICTURES FOLDER
    picData = {
      picture: formFiles.picture.name,
    }
    preparedData = Object.assign({}, standartData, picData)
    return preparedData
  }
}
