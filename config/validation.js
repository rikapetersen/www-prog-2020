const { check } = require('express-validator/check')

module.exports = {
  // - - - CHECK THE INPUT FROM REGISTRATIONFORM - - -
  validateChecks: [
    check('user_name')
      .isLength({ min: 2 })

      .withMessage('Benutzername muss ausgefüllt werden!')

      .isAlphanumeric()

      .withMessage(
        'Benutzername muss aus Buchstaben und Zahlen bestehen! Hast du vielleicht einen Umlaut verwendet? Umlaute im Usernamen sind leider nicht möglich :(',
      ),

    check('user_email')
      .matches('@')

      .withMessage(
        'Bitte eine gültige E-Mail Adresse eingeben! Zum Beispiel maxmustermann@aol.de',
      ),

    check('user_password')
      .isLength({ min: 8 })
      .withMessage('Ihr Passwort muss mindestens acht Zeichen enthalten')

      .matches('[0-9]')
      .withMessage('Ihr Passwort muss mindestens eine Zahl enthalten.')

      .matches('[a-z]')
      .withMessage(
        'Ihr Passwort muss mindestens einen Kleinbuchstaben enthalten.')

      .matches('[A-Z]')
      .withMessage(
        'Ihr Passwort muss mindestens einen Großbuchstaben enthalten.')

      .custom((value, { req, loc, path }) => {
        if (value !== req.body.passwordmatch) {
          return false
        } else {
          return value
        }
      })
      .withMessage(
        'Achtung! Die beiden eingegebenen Passwörter müssen identisch sein!',
      ),
  ],
}
